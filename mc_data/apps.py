from django.apps import AppConfig


class McDataConfig(AppConfig):
    name = 'mc_data'
