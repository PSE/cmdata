from import_export import resources, fields
from import_export.widgets import ForeignKeyWidget, ManyToManyWidget

from .models import Spermogram

# Register your models here.


class SpermogramResource(resources.ModelResource):
    class Meta:
        model = Spermogram
        fields = ("mc_user", "date", "registered_by", "concentration", "mobility", "typical_form")