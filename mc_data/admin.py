from django.contrib import admin
from .models import Spermogram, BiologicalExam, ClinicalExam, \
    SideEffect, Motivation,\
    AdditionalPersonalData, SewingWorkshop, SexualBehaviorPeriod
from .filters import DropdownFilterAll, DropdownFilterRel, DropdownFilterSimple

# Register your models here.


class MCAdmin(admin.ModelAdmin):
    exclude = ('registered_by', 'registered_at')
    def save_model(self, request, obj, form, change):
        if obj.registered_by is None:
            obj.registered_by = request.user
        super().save_model(request, obj, form, change)


class SewingWorkshopAdmin(MCAdmin):
    list_display = ("mc_user", "date", "registered_by")

    ordering = ('-date', 'mc_user')
    list_filter = (
        ('mc_user', DropdownFilterRel),
        ('date', DropdownFilterAll),
        ('registered_by', DropdownFilterRel),
    )


class AdditionalPersonalDataAdmin(MCAdmin):
    list_display = ("mc_user", "registered_by")

    ordering = ('mc_user',)
    list_filter = (
        ('mc_user', DropdownFilterRel),
        ('registered_by', DropdownFilterRel),
    )


class SideEffectAdmin(MCAdmin):
    list_display = ("description", "registered_by")

    ordering = ('description',)
    list_filter = (
        ('description', DropdownFilterAll),
        ('registered_by', DropdownFilterRel),
    )


class MotivationAdmin(MCAdmin):
    list_display = ("description", "registered_by")

    ordering = ('description',)
    list_filter = (
        ('description', DropdownFilterAll),
        ('registered_by', DropdownFilterRel),
    )


class ClinicalExamAdmin(MCAdmin):
    list_display = ("mc_user", "date", "registered_by")

    ordering = ('-date', 'mc_user')
    list_filter = (
        ('mc_user', DropdownFilterRel),
        ('date', DropdownFilterAll),
        ('registered_by', DropdownFilterRel),
    )


class BiologicalExamAdmin(MCAdmin):
    list_display = ("mc_user", "date", "registered_by", "normal")

    ordering = ('-date', 'mc_user')
    list_filter = (
        ('mc_user', DropdownFilterRel),
        ('date', DropdownFilterAll),
        ('registered_by', DropdownFilterRel),
        ('normal', DropdownFilterAll),
    )


class SpermogramAdmin(MCAdmin):
    list_display = ("mc_user", "date", "registered_by", "concentration", "mobility", "typical_form")

    ordering = ('-date', 'mc_user')
    list_filter = (
        ('mc_user', DropdownFilterRel),
        ('registered_by', DropdownFilterRel),
        ('date', DropdownFilterAll),
        ('concentration', DropdownFilterAll),
    )


class SexualBehaviorPeriodAdmin(MCAdmin):
    list_display = ("mc_user", "starting_date", "ending_date",
                    "registered_by")

    ordering = ('-starting_date', 'mc_user')
    list_filter = (
        ('mc_user', DropdownFilterRel),
        ('registered_by', DropdownFilterRel),
        ('starting_date', DropdownFilterAll),
    )


admin.site.register(Spermogram, SpermogramAdmin)
admin.site.register(BiologicalExam, BiologicalExamAdmin)
admin.site.register(ClinicalExam, ClinicalExamAdmin)
admin.site.register(Motivation,MotivationAdmin)
admin.site.register(SideEffect, SideEffectAdmin)
admin.site.register(AdditionalPersonalData, AdditionalPersonalDataAdmin)
admin.site.register(SewingWorkshop, SewingWorkshopAdmin)
admin.site.register(SexualBehaviorPeriod, SexualBehaviorPeriodAdmin)
