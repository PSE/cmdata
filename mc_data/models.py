from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator
from django_date_extensions.fields import ApproximateDateField
from django.core.validators import RegexValidator
from django_countries.fields import CountryField

# ApproximateDateField can be either
# 'YYYY-MM-DD' or 'YYYY-MM-00' or 'YYYY-00-00' or 'future' or 'past'


class MaleMethod:
    OTHER = 'O'
    THERMAL = 'T'
    HORMONAL = 'H'
    VASECTOMY = 'V'
    CHOICES = (
        (OTHER, _('Other')),
        (HORMONAL, _('Hormonal')),
        (VASECTOMY, _('Vasectomy')),
        (THERMAL, _('Thermal')),
    )


class ThermalMethod:
    JOCKSTRAP = 'J'
    PANTS = 'P'
    RING = 'R'
    SPERMAPAUSE = 'SP'
    OTHER = 'O'
    CHOICES = (
        (OTHER, _('Other')),
        (JOCKSTRAP, _('Jockstrap')),
        (RING, _('Ring')),
        (PANTS, _('Pants')),
        (SPERMAPAUSE, _('Spermapause'))
    )


class SharedMethod:
    OTHER = 'O'
    MALE_CONDOM = 'C'
    FEMALE_CONDOM = 'FC'
    WITHDRAWAL = 'W'
    ABSTINENCE = 'A'

    CHOICES = (
        (OTHER, _('Other')),
        (MALE_CONDOM, _('Male condom')),
        (FEMALE_CONDOM, _('Female condom')),
        (WITHDRAWAL, _('Withdrawal')),
        (ABSTINENCE, _('Abstinence')),
    )


class FemaleMethod:
    OTHER = 'O'
    PILL = 'Pi'
    IUD = 'IUD'
    VAGINAL_RING = 'VR'
    IMPLANT = 'I'
    PATCH = 'Pa'
    DIAPHRAGM = 'D'
    DEPO_PROVERA = 'DP'
    NATURAL_METHODS = 'NM'

    CHOICES = (
        (OTHER, _('Other')),
        (PILL, _('Pill')),
        (IUD, _('IUD')),
        (VAGINAL_RING, _('Vaginal ring')),
        (PATCH, _('Patch')),
        (DIAPHRAGM, _('Diaphragm')),
        (DEPO_PROVERA, _('Depo-provera')),
        (NATURAL_METHODS, _('Natural methods'))
    )


class CustomModel(models.Model):
    registered_by = models.ForeignKey('userauth.CustomUser',
                                      on_delete=models.SET_NULL,
                                      null=True)
    registered_at = models.DateField(auto_now=True)
    comment = models.TextField(null=True, blank=True, verbose_name=_("Comment"))

    class Meta:
        abstract = True

    @classmethod
    def get_ordered_fields(cls):
        fields = [f.name for f in cls._meta.fields if f.name!='comment']
        fields.append('comment')
        return fields


class MCPeriod(CustomModel):
    mc_user = models.ForeignKey('userauth.MCUser', on_delete=models.CASCADE)
    starting_date = ApproximateDateField(verbose_name=_("Starting (approximate) date"), default="past",
                                         help_text=_("YYYY-MM-DD or YYYY-MM-00 or YYYY-00-00 or future or past"))
    ending_date = ApproximateDateField(verbose_name=_("Ending (approximate) date"), default="future",
                                       help_text=_("YYYY-MM-DD or YYYY-MM-00 or YYYY-00-00 or future or past"))

    class Meta:
        db_table = 'period_data'

    def __str__(self):
        return f"{self.mc_user} [{self.starting_date}-{self.ending_date}]: {self.comment}"


class SexualBehaviorPeriod(MCPeriod):
    fertilizing = models.BooleanField(null=True, blank=True, verbose_name=_("Fertilizing ?"))
    sti_risk = models.BooleanField(null=True, blank=True, verbose_name=_("STI risk ?"))
    contraceptive_methods = models.ManyToManyField("ContraceptiveMethod", verbose_name=_("Contraceptive methods"))

    class Meta:
        verbose_name = _('Sexual behaviour period')


class ThermalContraceptionPeriod(MCPeriod):
    method = models.CharField(max_length=2, choices=ThermalMethod.CHOICES,
                              verbose_name=_("Method"))
    waking_hours_per_day = models.PositiveSmallIntegerField(validators=[MaxValueValidator(24)],
                                                            verbose_name=_('Waking hours per day'), default=0)
    sleeping_hours_per_day = models.PositiveSmallIntegerField(validators=[MaxValueValidator(24)],
                                                              verbose_name=_('Sleeping hours per day'), default=0)

    class Meta:
        verbose_name = _('Thermal contraception period')


class SideEffectPeriod(MCPeriod):
    side_effects = models.ManyToManyField('SideEffect', verbose_name=_("Side effects"))

    class Meta:
        verbose_name = _('Side-effect period')


class SmokingPeriod(MCPeriod):
    cigarettes_per_day = models.PositiveSmallIntegerField(validators=[MaxValueValidator(24)],
                                                          verbose_name=_('Cigarettes per day'), default=0)

    class Meta:
        verbose_name = _('Smoking period')


class MCEvent(CustomModel):
    mc_user = models.ForeignKey('userauth.MCUser', on_delete=models.CASCADE)
    date = ApproximateDateField(verbose_name=_("(Approximate) Date"), default="past",
                                help_text=_("YYYY-MM-DD or YYYY-MM-00 or YYYY-00-00 or future or past"))

    class Meta:
        db_table = 'event_data'

    def __str__(self):
        return f"{self.mc_user} - {self.date}: {self.comment}"


class Exam(MCEvent):
    prescriber = models.ForeignKey('userauth.Doctor', on_delete=models.SET_NULL, blank=True, null=True,
                                   verbose_name=_("Prescriber"))

    class Meta:
        abstract = True


class Spermogram(Exam):
    laboratory = models.ForeignKey('Laboratory', on_delete=models.SET_NULL, blank=True, null=True,
                                   verbose_name=_("Laboratory"))
    concentration = models.DecimalField(verbose_name=_("Concentration"), max_digits=5, decimal_places=2,
                                        blank=True, null=True)
    mobility = models.PositiveSmallIntegerField(verbose_name=_("Mobility"), validators=[MaxValueValidator(100)],
                                                blank=True, null=True)
    typical_form = models.PositiveSmallIntegerField(verbose_name=_("Typical form"),
                                                    validators=[MaxValueValidator(100)],
                                                    blank=True, null=True)
    abstinence_delay = models.PositiveSmallIntegerField(verbose_name=_('Nb of abstinence days'),
                                                        blank=True, null=True)
    last_ejaculation = models.DateField(verbose_name=_('Last ejaculation'),
                                        blank=True, null=True)
    class Meta:
        verbose_name = _("Spermogram")


class BiologicalExam(Exam):
    laboratory = models.ForeignKey('Laboratory', on_delete=models.SET_NULL, blank=True, null=True,
                                   verbose_name=_("Laboratory"))
    ht = models.DecimalField(verbose_name=_("HT"), max_digits=3, decimal_places=1,
                             validators=[MaxValueValidator(100)],
                             blank=True, null=True)
    cgt = models.PositiveSmallIntegerField(verbose_name=_("CGT"), validators=[MaxValueValidator(200)],
                                           blank=True, null=True)
    asat = models.PositiveSmallIntegerField(verbose_name=_("ASAT"), validators=[MaxValueValidator(200)],
                                            blank=True, null=True)
    alat = models.PositiveSmallIntegerField(verbose_name=_("ALAT"), validators=[MaxValueValidator(200)],
                                            blank=True, null=True)
    ca = models.PositiveSmallIntegerField(verbose_name=_("CA"), validators=[MaxValueValidator(100)],
                                          blank=True, null=True)
    ldl = models.PositiveSmallIntegerField(verbose_name=_("LDL"), validators=[MaxValueValidator(100)],
                                           blank=True, null=True)
    hdl = models.PositiveSmallIntegerField(verbose_name=_("HDL"), validators=[MaxValueValidator(100)],
                                           blank=True, null=True)
    tg = models.PositiveSmallIntegerField(verbose_name=_("TG"), validators=[MaxValueValidator(100)],
                                          blank=True, null=True)
    normal = models.BooleanField(default=True)

    class Meta:
        verbose_name = _('Biological exam')


class ClinicalExam(Exam):
    bmi = models.PositiveSmallIntegerField(verbose_name=_("BMI"), validators=[MaxValueValidator(100)],
                                           blank=True, null=True)
    systolic_pressure = models.PositiveSmallIntegerField(verbose_name=_("Systolic pressure"),
                                                         validators=[MaxValueValidator(200)],
                                                         blank=True, null=True)
    diastolic_pressure = models.PositiveSmallIntegerField(verbose_name=_("Diastolic pressure"),
                                                          validators=[MaxValueValidator(100)],
                                                          blank=True, null=True)

    def get_tension(self):
        return f"{self.systolic_pressure}/{self.diastolic_pressure}"

    class Meta:
        verbose_name = _("Clininal exam")


class MCData(CustomModel):
    name = models.CharField(max_length=16, default='NC', verbose_name=_("Name"))
    description = models.CharField(max_length=64, null=True, blank=True, verbose_name=_("Description"))

    def __str__(self):
        return self.description

    class Meta:
        abstract = True


class ContraceptiveMethod(MCData):
    MALE = 'M'
    FEMALE = 'F'
    SHARED = 'S'
    CHOICES = ((MALE, _('Male')),
               (FEMALE, _('Female')),
               (SHARED, _('Shared')))
    gender = models.CharField(max_length=1, choices=CHOICES)

    def __str__(self):
        return f'{self.gender}: {self.description}'


class SideEffect(MCData):
    pass

    class Meta:
        verbose_name = _("Side effect")


class Value:
    NO = 0
    LOW = 1
    MODERATE = 2
    HIGH = 3
    VERY_HIGH = 4
    EXTREME = 5

    CHOICES = (
        (NO, _('Not at all')),
        (LOW, _('Low')),
        (MODERATE, _('Moderate')),
        (HIGH, _('High')),
        (VERY_HIGH, _('Very high')),
        (EXTREME, _('Extreme'))
    )


class Motivation(MCData):

    def save(self, *args, **kwargs):
        super().save(self, *args, **kwargs)
        if not ValuedMotivation.objects.filter(motivation=self).exists():
            for value in range(6):
                ValuedMotivation(motivation=self, value=value).save()



class ValuedMotivation(models.Model):

    motivation = models.ForeignKey('Motivation',  on_delete=models.CASCADE, verbose_name=_("Motivation"))
    value = models.PositiveSmallIntegerField(choices=Value.CHOICES,
                                             null=True, blank=True, verbose_name=_("Value"))

    def __str__(self):
        return f"{self.motivation}: {self.value}"


class Meeting(CustomModel):
    mc_user = models.ForeignKey('userauth.MCUser', on_delete=models.CASCADE)
    date = ApproximateDateField(verbose_name=_("(Approximate) Date"), default="past",
                                help_text=_("YYYY-MM-DD or YYYY-MM-00 or YYYY-00-00 or future or past"))


class RelationalMode:
    STABLE = 'S'
    MULTIPLE = 'M'
    BOTH = 'S+M'
    OTHER = 'O'
    CHOICES = ((STABLE, _('Stable')),
               (MULTIPLE, _('Multiple')),
               (BOTH, _('Stable and Multiple')),
               (OTHER, _('Other'))
               )


class AdditionalPersonalData(CustomModel):
    mc_user = models.ForeignKey('userauth.MCUser', on_delete=models.CASCADE)
    motivations = models.ManyToManyField('ValuedMotivation')
    relational_mode = models.CharField(max_length=3, choices=RelationalMode.CHOICES,
                                       null=True, blank=True,
                                       verbose_name=_("Relational mode"))
    children_nb = models.PositiveSmallIntegerField(default=0,
                                                   verbose_name=_("Nb of children"))
    abortion_nb = models.PositiveSmallIntegerField(default=0,
                                                   verbose_name=_("Nb of abortions"))
    sti_risk_acceptance = models.PositiveSmallIntegerField(default=0,
                                                           validators=[MaxValueValidator(100)],
                                                           verbose_name=_("STI-risk acceptance (in %)"))
    pregnancy_risk_acceptance = models.PositiveSmallIntegerField(default=0,
                                                                 validators=[MaxValueValidator(100)],
                                                                 verbose_name=_("Pregnancy-risk acceptance (in %)"))
    unreversibility_risk_acceptance = models.PositiveSmallIntegerField(default=0,
                                                                       validators=[MaxValueValidator(100)],
                                                                       verbose_name=_("Unreversibility-risk acceptance (in %)"))
    contraindications = models.ManyToManyField("ValuedContraindication")

    class Meta:
        verbose_name = _('Additional personal data')

    # Check_list:
    # - Attention : différence si c'est couple stable ou partenaires multiples
    # - Questions sur les pratiques sexuelles: fécondantes ou non, protégées ou non, rétention de sperme, ...
    # - Information sur chaque méthode masculine (+ liste des autres comme support)
    # et sur la complémentarité des méthodes (+texte d explication)
    # - Implication pour la/les partenaire(s) : question, incitation à une consultation à 2, informations...
    # - Questions sur le niveau de prise de risque vis à vis des IST,
    # et d'acceptation du risque de grossesse non-désirée, ...
    # - Choix de la/des méthodes.
    # - Pour chaque méthode choisies
    #     - Contre-indications
    #     - Bonne compréhension du protocole (+ descriptif)
    #     - Effets secondaires possibles et précautions à prendre
    #     - Réversibilité et envie/besoin de recueil préalable de sperme (et légalité, et coût, ...)


class FinalMedicalMeeting(Meeting):
    pass


class SewingWorkshop(Meeting):
    animator = models.ForeignKey("userauth.Activist", on_delete=models.SET_NULL, null=True)
    motivations = models.ManyToManyField('ValuedMotivation')

    class Meta:
        verbose_name = _('Sewing workshop')


class BasicContraindication:
    ECTOPY = 'E'
    CRYPTORCHIDISM = 'C'
    INGUINAL_HERNIA = 'IH'
    OBESITY = 'OB'
    VARICOCELE = 'V'
    UNNATURAL_SPERMOGRAM = 'US'
    OTHER = 'O'
    CHOICES = (
        (ECTOPY, _('Ectopy')),
        (CRYPTORCHIDISM, _('Chryptorchidism')),
        (INGUINAL_HERNIA, _('Inguinal hernia')),
        (OBESITY, _('Obesity')),
        (VARICOCELE, _('Varicocele')),
        (UNNATURAL_SPERMOGRAM, _('Unnatural spermogram')),
        (OTHER, _('Other'))
    )


class Contraindication(MCData):

    def save(self, *args, **kwargs):
        super().save(self, *args, **kwargs)
        if not ValuedContraindication.objects.filter(contraindication=self).exists():
            for value in range(11):
                ValuedContraindication(contraindication=self, value=value).save()

    class Meta:
        verbose_name = _('Contraindication')


class ValuedContraindication(models.Model):
    contraindication = models.ForeignKey('Contraindication', on_delete=models.CASCADE,
                                         verbose_name=_('Contraindication'))
    value = models.PositiveSmallIntegerField(choices=Value.CHOICES,
                                             null=True, blank=True,
                                             verbose_name=_('Value'))

    def __str__(self):
        return f"{self.contraindication}: {self.value}"


class Disease(MCData):

    class Meta:
        verbose_name = _('Disease')


class DiseaseOutbreak(MCEvent):
    disease = models.ForeignKey('Disease', on_delete=models.CASCADE,
                                verbose_name=_('Disease'))

    class Meta:
        verbose_name = _('Disease outbreak')


class Pregnancy(MCEvent):
    desired = models.BooleanField()

    class Meta:
        verbose_name = _('Pregnancy')


class Vasectomy(MCEvent):
    doctor = models.ForeignKey('userauth.Doctor', on_delete=models.SET_NULL,
                               null=True, blank=True)

    class Meta:
        verbose_name = _('Vasectomy')


class MedicalInstitution(models.Model):
    name = models.CharField(max_length=32)
    description = models.CharField(max_length=64, null=True, blank=True)
    address1 = models.CharField(verbose_name=_("Address line 1"), max_length=1024, blank=True, null=True)
    address2 = models.CharField(verbose_name=_("Address line 2"), max_length=1024, blank=True, null=True)
    zip_code = models.CharField(verbose_name=_("Postal Code"), max_length=12, blank=True, null=True)
    city = models.CharField(verbose_name=_("City"), max_length=32, blank=True, null=True)
    country = CountryField(blank=True, null=True, verbose_name=_("Country"))
    phone_regex = RegexValidator(regex=r"^\+(?:[0-9]●?){6,14}[0-9]$",
                                 message=_("Enter a valid international mobile phone number "
                                           "starting with +(country code)"))
    mobile_phone = models.CharField(validators=[phone_regex], verbose_name=_("Mobile phone"), max_length=17, blank=True,
                                    null=True)
    additional_information = models.CharField(verbose_name=_("Additional information"), max_length=4096, blank=True,
                                              null=True)
    logo = models.ImageField(verbose_name=_("Logo"), upload_to='photos/', default='photos/default-user-avatar.png')
    comment = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return f"{self.name} - {self.city}"


class Laboratory(MedicalInstitution):

    class Meta:
        verbose_name = _('Laboratory')


class Hospital(MedicalInstitution):
    pass

    class Meta:
        verbose_name = _('Hospital')


def add_all_contraceptive_methods():
    for m in MaleMethod.CHOICES:
        if m[0] == 'T':
            for tm in ThermalMethod.CHOICES:
                name = f'{m[0]}-{tm[0]}'
                description = f'{[0]}-{tm[1]}'
                ContraceptiveMethod(name=name, description=description, gender='M').save()
        else:
            name = m[0]
            description = m[1]
            ContraceptiveMethod(name=name, description=description, gender='M').save()
    for m in FemaleMethod.CHOICES:
        name = m[0]
        description = m[1]
        ContraceptiveMethod(name=name, description=description, gender='F').save()
    for m in SharedMethod.CHOICES:
        name = m[0]
        description = m[1]
        ContraceptiveMethod(name=name, description=description, gender='S').save()


def add_all_basic_contraindications():
    for bc in BasicContraindication.CHOICES:
        name = bc[0]
        description = bc[1]
        Contraindication(name=name, description=description).save()
