# Generated by Django 3.1.7 on 2021-03-28 15:19

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('userauth', '0002_auto_20210318_1012'),
        ('mc_data', '0005_auto_20210325_1342'),
    ]

    operations = [
        migrations.AddField(
            model_name='clinicalexam',
            name='prescriber',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='userauth.doctor'),
        ),
        migrations.AlterField(
            model_name='additionalpersonaldata',
            name='abortion_nb',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Nb of abortions'),
        ),
        migrations.AlterField(
            model_name='additionalpersonaldata',
            name='children_nb',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Nb of children'),
        ),
        migrations.AlterField(
            model_name='additionalpersonaldata',
            name='relational_mode',
            field=models.CharField(blank=True, choices=[('S', 'Stable'), ('M', 'Multiple'), ('S+M', 'Stable and Multiple'), ('O', 'Other')], max_length=3, null=True, verbose_name='Relational mode'),
        ),
        migrations.AlterField(
            model_name='diseaseoutbreak',
            name='disease',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mc_data.disease', verbose_name='Disease'),
        ),
        migrations.AlterField(
            model_name='medicalinstitution',
            name='country',
            field=django_countries.fields.CountryField(blank=True, max_length=2, null=True, verbose_name='Country'),
        ),
        migrations.AlterField(
            model_name='valuedcontraindication',
            name='contraindication',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mc_data.contraindication', verbose_name='Contraindication'),
        ),
        migrations.AlterField(
            model_name='valuedcontraindication',
            name='value',
            field=models.PositiveSmallIntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(5)], verbose_name='Value'),
        ),
    ]
