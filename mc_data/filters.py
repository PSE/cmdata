from django.contrib.admin.filters import AllValuesFieldListFilter, RelatedFieldListFilter, \
                                        ChoicesFieldListFilter, RelatedOnlyFieldListFilter
from django.db.models.fields import BLANK_CHOICE_DASH


class DropdownFilterAll(AllValuesFieldListFilter):
    template = 'admin/dropdown_filter.html'


class DropdownFilterRel(RelatedOnlyFieldListFilter):
    template = 'admin/dropdown_filter.html'


class DropdownFilterSimple(ChoicesFieldListFilter):
    template = 'admin/dropdown_filter.html'
