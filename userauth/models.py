from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from faker import Faker

from django_countries.fields import CountryField


class CustomUser(AbstractUser):
    display_name = models.CharField(verbose_name=_("Display name"), max_length=30,
                                    help_text=_("Will be shown e.g. when commenting"))
    date_of_birth = models.DateField(verbose_name=_("Date of birth"), blank=True, null=True)
    address1 = models.CharField(verbose_name=_("Address line 1"), max_length=1024, blank=True, null=True)
    address2 = models.CharField(verbose_name=_("Address line 2"), max_length=1024, blank=True, null=True)
    zip_code = models.CharField(verbose_name=_("Postal Code"), max_length=12, blank=True, null=True)
    city = models.CharField(verbose_name=_("City"), max_length=1024, blank=True, null=True)
    country = CountryField(blank=True, null=True)
    phone_regex = RegexValidator(regex=r"^\+(?:[0-9]●?){6,14}[0-9]$",
                                 message=_("Enter a valid international mobile phone number "
                                           "starting with +(country code)"))
    mobile_phone = models.CharField(validators=[phone_regex], verbose_name=_("Mobile phone"), max_length=17, blank=True,
                                    null=True)
    ine_regex = RegexValidator(regex=r'^'
                                     '[12]'
                                     '[0-9]{2}'
                                     '(0[1-9]|1[0-2])'
                                     '(2[AB]|[0-9]{2})'
                                     '[0-9]{6}'
                                     '[0-9]{2}'
                                     '$',
                               message=_("Enter a valid INE number"))
    ine_number = models.CharField(validators=[ine_regex], verbose_name=_("INE number"),
                                  max_length=15, blank=True, null=True)
    additional_information = models.CharField(verbose_name=_("Additional information"), max_length=4096, blank=True,
                                              null=True)
    photo = models.ImageField(verbose_name=_("Photo"), upload_to='photos/', default='photos/default-user-avatar.png')

    roles = models.ManyToManyField('Role', related_name='users')

    class Meta:
        ordering = ['last_name']
        verbose_name = _('Custom user')

    def get_absolute_url(self):
        return reverse('account_profile')

    def __str__(self):
        return f"{self.username}"


class Role(models.Model):
    """
    The Role entries are managed by the system,
    automatically created via a Django data migration.
    """
    MC_USER = 1
    DOCTOR = 2
    ACTIVIST = 3
    WEB_ADMIN = 4
    JOURNALIST = 5
    ROLE_CHOICES = (
        (MC_USER, _('Male contraception user')),
        (DOCTOR, _('Doctor')),
        (ACTIVIST, _('Activist')),
        (WEB_ADMIN, _('Web administrator')),
        (JOURNALIST, _('Journalist')),
    )

    id = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, primary_key=True)

    def __str__(self):
        return self.get_id_display()


class MCUser(models.Model):
    user = models.OneToOneField('CustomUser', on_delete=models.CASCADE, related_name='as_mc_user')
    fake_first_name = models.CharField(max_length=20, null=True)
    fake_last_name = models.CharField(max_length=20, null=True)

    def __str__(self):
        return f"{self.user.username}"

    class Meta:
        verbose_name = _('Male contraception user')

    def save(self, *args, **kwargs):
        if not self.pk:
            faker = Faker(['fr_FR', 'it_IT'])
            self.fake_first_name = faker.first_name_male()
            self.fake_last_name = faker.last_name()
        super(MCUser, self).save(*args, **kwargs)


class Partner(models.Model):
    user = models.OneToOneField('CustomUser', on_delete=models.CASCADE, related_name='as_partner')
    mc_user = models.ForeignKey('CustomUser', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user.username}"

    class Meta:
        verbose_name = _('Partner')


class Doctor(models.Model):
    user = models.OneToOneField('CustomUser', on_delete=models.CASCADE, related_name='as_doctor')

    def __str__(self):
        return f"{self.user.username}"

    class Meta:
        verbose_name = _('Doctor')


class Activist(models.Model):
    user = models.OneToOneField('CustomUser', on_delete=models.CASCADE, related_name='as_activist')
    organizations = models.ManyToManyField('Organization', through='ActivistSettings')

    def __str__(self):
        return f"{self.user.username} - {self.organizations.all()}"

    class Meta:
        verbose_name = _('Activist')


class ActivistSettings(models.Model):
    """
    This model allows to add additionnal settings to the
    relation between Activist and Organization
    """
    activist = models.ForeignKey('Activist', on_delete=models.CASCADE)
    organization = models.ForeignKey('Organization', on_delete=models.CASCADE)
    is_main = models.BooleanField(default=False)
    leader = models.BooleanField(default=False)

    def __str__(self):
        return (f'U:{self.activist.user.username}, O:{self.organization.name}, '
                f'{"leader" if self.leader else "activist"}')

    class Meta:
        verbose_name = _('Activist settings')


class Organization(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=256)
    abbreviation = models.CharField(verbose_name=_("Abbreviation"), max_length=8)

    class Meta:
        verbose_name = _('Organization')
