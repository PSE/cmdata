from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test

def doctor_required(view_func=None, redirect_field_name=REDIRECT_FIELD_NAME,
                   login_url='account_login'):
    """
    Decorator for views that checks that the user is a tutor,
    redirecting to the login page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda u: not u.is_anonymous and hasattr(u, 'as_doctor'),
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if view_func:
        return actual_decorator(view_func)
    return actual_decorator
