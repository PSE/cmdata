# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## TBD
- Hide motivations/contraindications when removed
- Better CSS (e.g. motivations/contraindications)

## Known bugs
- @login_required: does not redirect to the right page after login
  page. Comes from 'allauth' module, I guess.

## Added
- Medical exams
- User form typed by a doctor
- Initial meeting forms
