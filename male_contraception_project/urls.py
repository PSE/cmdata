from django.conf import settings
from django.conf.urls import include, url
from django.urls import path, re_path
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from search import views as search_views
from django.views.i18n import JavaScriptCatalog
import consultation.views as consultation_views

urlpatterns = [
    path(r'', consultation_views.index),
    path('django-admin/', admin.site.urls),

    path('admin/', include(wagtailadmin_urls)),
    path('documents/', include(wagtaildocs_urls)),

    path('search/', search_views.search, name='search'),
    path('api/', include('api.urls')),
]

urlpatterns += i18n_patterns(
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
    path('accounts/', include('allauth.urls')),
    path('accounts/', include('userauth.urls')),
    path('language/', include('cms.urls')),
    path('comments/', include('django_comments_xtd.urls')),
    path('consultation/', include('consultation.urls')),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# removed for now, otherwise cannot see which urls django tried
# ---
# urlpatterns = urlpatterns + [
#     # For anything not caught by a more specific rule above, hand over to
#     # Wagtail's page serving mechanism. This should be the last pattern in
#     # the list:
#     url(r"", include(wagtail_urls)),

#     # Alternatively, if you want Wagtail pages to be served from a subpath
#     # of your site, rather than the site root:
#     #    url(r"^pages/", include(wagtail_urls)),
# ]
