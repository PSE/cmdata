from django.urls import path

from consultation.views import pick, start, save_additional_personal_data, save_exam,\
    save_period

app_name = 'consultation'

urlpatterns = [
    path('pick', pick, name='pick'),
    path('start/<int:pk_mcuser_as_user>', start, name='start'),
    path('save_additional_personal_data/<int:pk_mcuser>',
         save_additional_personal_data,
         name='save_additional_personal_data'),
    path('save_exam', save_exam, name='save_exam'),
    path('save_period', save_period, name='save_period'),
]
