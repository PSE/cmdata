

$("#search-button").click(function() {
  let text = $("#search-field").val() ;
  console.log(text);

  $.ajax({
    type: "GET",
    dataType: 'text',
    url: url_approx_search + "?approx_last_name=" + text,
    async: true,
    contentType: "application/json",
    success: function(msg, ts, req) {
      build_table(JSON.parse(msg));
    }
  });

});


function build_table(user_list) {
  $("#table-head").css('visibility', 'visible') ;

  $("#table-body").empty();
  
  user_list.forEach(function(u){
    let line = "<tr class='crow' data-href='" + url_start + u.id + "'>" ;
    line += cell(u.first_name)  ;
    line += cell(u.last_name) ;
    line += cell(u.date_of_birth) ;
    line += '</tr></a>';
    console.log(line);
    $("#table-body").append(line);
  }
  );

  $(".crow").click(function() {
    window.location = $(this).data("href");
  });

}

function cell(s) {
  return '<td>' + (s==''?'---':s) + '</td>';
}

jQuery(document).ready(function($) {
})
