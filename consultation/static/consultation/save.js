$('#btn_comp_save').on('click', function() {
  let comp_data = $("#personal_data_form").serializeArray();
  for (let ifs = 0 ; ifs < 2 ; ifs ++) {
    comp_data = comp_data.concat($("#" + formset_list[ifs].prefix).serializeArray()) ;
  }
  $.ajax({
    type: "POST",
    headers: {'X-CSRFToken': csrftoken},
    url: url_save_additional_personal_data,
    data: comp_data,
    success: function (data) {
      console.log('success');
    },
    error: function (data) {
      console.log('error');
    },
  });

});


function send_formset(fs) {
  let data2send = [] ;
  data2send = data2send
    .concat($("#" + fs.prefix).serializeArray())
    .filter(function(d) {
      const regexp = new RegExp(fs.prefix + '-([0-9]+)-.*') ;
      const caught = d.name.match(regexp) ;
      return (caught == null || +caught[1] < fs.nb) ;
    });
  console.log(data2send);
  $.ajax({
    type: "POST",
    headers: {'X-CSRFToken': csrftoken},
    url: fs.url_post,
    data: data2send,
    success: function (data) {
      console.log('success');
    },
    error: function (data) {
      console.log('error');
    },
  });
}

formset_list.forEach(function(fs) {
  if (fs.url_post != '') {
    $('#btn_save_' + fs.prefix).on('click', function() {
      send_formset(fs);
    });
  }
})
                     
