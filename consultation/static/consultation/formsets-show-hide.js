for (let ifs = 0 ;  ifs < formset_list.length ; ifs ++) {
  const partial = '#' + formset_list[ifs].prefix + ' #id_' + formset_list[ifs].prefix ;
  const nb_init = +$(partial + '-INITIAL_FORMS').attr('value') ;
  const nb_tot = +$(partial + '-TOTAL_FORMS').attr('value') ;
  console.log(partial, nb_init, nb_tot);

  // hide unfilled forms
  $('#' + formset_list[ifs].prefix).children('div').each(function(i){
    if(i >= nb_init && i < nb_tot) {
      $(this).hide() ;
    }
  })

  formset_list[ifs].nb = nb_init ;
  $(partial + '-TOTAL_FORMS').attr('value', nb_init) ;
  
  $('#btn_more_' + formset_list[ifs].prefix).on('click', function() {
    $('#' + formset_list[ifs].prefix).children('div').each(function(i){
      if(i == formset_list[ifs].nb) {
        $(this).show() ;
      }
    });
    formset_list[ifs].nb ++ ;
    $('#' + formset_list[ifs].prefix + ' #id_' + formset_list[ifs].prefix
      + '-TOTAL_FORMS')
      .attr('value', formset_list[ifs].nb);
  });

}
