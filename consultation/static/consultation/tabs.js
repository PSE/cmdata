function hide_tab_contents () {
  $('.tab-content').each(function(){
    $(this).hide();
  });
}
  
function open_tab(event, tab_id) {
  tab_id = '#' + tab_id ;

  // hide tab and deactivate links
  hide_tab_contents();
  $('.tab-link').each(function(){
    $(this).removeClass('active');
  })

  // Show current tab, activate link
  $(tab_id).show() ;
  event.currentTarget.className += " active";
}


// first hide all tab contents
hide_tab_contents();
