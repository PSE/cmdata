$('#btn_ser_user').on('click', put_user_form);

function put_user_form() {
  const form_id = 'ser_user' ;
  let user_data = pack_data(form_id, false);

  console.log(user_data);

  $.ajax({
    type: "PUT",
    headers: {'X-CSRFToken': csrftoken},
    url: url_update_user,
    data: user_data,
    success: function (data) {
      display_success(form_id);
    },
    error: function (data) {
      display_errors(form_id, data.responseJSON);
    },
  });
}

function display_errors(form_id, problem_data) {
  console.log('pas youpi');
  const map_problems = new Map(Object.entries(problem_data));
  let problems = '' ;
  for (const [pb_type, pb_list] of map_problems.entries()) {
    if (typeof pb_list === 'string') {
      problems += '<li>' + pb_type + ' : ' + pb_list + '</li>'
    } else {
      let pb_tmp = '' ;
      pb_list.forEach(function(p){
        pb_tmp += '<li>' + p + '</li>';
      });
      problems += '<li>Problem with ' + pb_type + ':<ul>' + pb_tmp + '</ul></li>' ;
    }
  }
  
  $("#" + form_id + " #error-list").html(
    '<ul>' + problems
      +'</ul>'
  );
  console.log(problems);
}

function display_success(form_id) {
  console.log('youpi');
  $("#" + form_id + " #error-list").html(
    '<p style="color:brown">Bien enregistré.</p>'
  );
}


// django_form: true|false
function pack_data(form_id, django_form) {
  let data_dict = {};
  $('#' + form_id + ' div input').each(function() {
    data_dict[this.name] = $(this).val();
    if (django_form) {
      data_dict[this.name] = data_dict[this.name].slice(3);
    }
  });
  $('#' + form_id + ' div select').each(function() {
    data_dict[this.name] = $(this).find(":selected").val();
  });
  return data_dict ;
}

