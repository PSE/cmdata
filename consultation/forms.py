from django import forms
from django.core.exceptions import ValidationError

from mc_data.models import AdditionalPersonalData, ValuedMotivation, \
    ValuedContraindication, Spermogram, BiologicalExam, ClinicalExam


class AdditionalPersonalDataForm(forms.ModelForm):

    class Meta:
        model = AdditionalPersonalData
        exclude = ('comment', 'mc_user', 'motivations', \
                   'contraindications', \
                   'registered_by', 'registered_at')


class ValuedMotivationForm(forms.ModelForm):

    class Meta:
        model = ValuedMotivation
        fields = '__all__'


class ValuedContraindicationForm(forms.ModelForm):

    class Meta:
        model = ValuedContraindication
        fields = '__all__'


class ValuedSomethingFormSet(forms.BaseFormSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.instances_to_del = []
        self.instances_to_add = []

    def clean(self):
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return

        titles = []

        for form in self.forms:
            if self.can_delete and self._should_delete_form(form):
                continue
            title = form.cleaned_data.get(self.Meta.att_name)
            if title in titles:
                raise ValidationError(f'One value per {something} '
                                      f'(here: at least 2 values for "{title}").')
            if title is not None:
                titles.append(title)


    def save(self, m2mfield):
        thing_to_del = []
        thing_to_add = []
        for form in self.forms:
            if form.cleaned_data.get('value') is not None:
                get_dict = form.cleaned_data.copy()
                to_del = get_dict.pop('DELETE')

                thing_to_del.append(self.Meta.model.objects.filter(
                    **{self.Meta.att_name: form.cleaned_data[self.Meta.att_name]}
                ))

                if not to_del:
                    thing_to_add.append(self.Meta.model.objects.get(**get_dict))


        for query in thing_to_del:
            for m in query:
                m2mfield.remove(m)
        for m in thing_to_add:
            m2mfield.add(m)
            


class ValuedMotivationFormSet(ValuedSomethingFormSet):

    class Meta:
        model = ValuedMotivation
        att_name = 'motivation'
        

class ValuedContraindicationFormSet(ValuedSomethingFormSet):

    class Meta:
        model = ValuedContraindication
        att_name = 'contraindication'
