from rest_framework import status

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import formset_factory, modelformset_factory, HiddenInput
from django.forms.models import model_to_dict
from django.contrib.auth.decorators import login_required
from django.urls import reverse

from userauth.models import MCUser, CustomUser
from userauth.decorators import doctor_required

from mc_data.models import ValuedMotivation, AdditionalPersonalData, Motivation,\
    ValuedContraindication, Contraindication, Exam, MCPeriod

from api.serializers import CustomUserSerializer

from consultation.forms import AdditionalPersonalDataForm, ValuedMotivationForm,\
    ValuedContraindicationForm, ValuedMotivationFormSet, \
    ValuedContraindicationFormSet

import consultation.forms as cforms

def index(request):
    return redirect('/fr/consultation/pick')


@doctor_required
def pick(request):
    return render(request, 'consultation/pick.html')


@doctor_required
def start(request, pk_mcuser_as_user):
    try:
        user = CustomUser.objects.get(pk=pk_mcuser_as_user)
    except CustomUser.DoesNotExist:
        return HttpResponse('Non-existing user.')

    user_serializer = CustomUserSerializer(user)

    mcuser = user.as_mc_user

    formset_list = ['motivation','contraindication']


    VmotivationsFormset = formset_factory(ValuedMotivationForm,
                                          formset=ValuedMotivationFormSet,
                                          extra=5,
                                          can_delete=True)
    VContraindicationFormset = formset_factory(
        ValuedContraindicationForm,
        formset=ValuedContraindicationFormSet,
        extra=5,
        can_delete=True
    )

    all_additional_personal_data = AdditionalPersonalData.objects.filter(mc_user=mcuser)
    if all_additional_personal_data.exists():
        additional_personal_data = all_additional_personal_data.last()
    else:
        additional_personal_data = AdditionalPersonalData(mc_user=mcuser)
        additional_personal_data.save()

    init_values_motiv = [model_to_dict(m) \
                         for m in additional_personal_data.motivations.all()]

    init_values_contra = [model_to_dict(c) \
                          for c in additional_personal_data.contraindications.all()]

    vmotivations_form = VmotivationsFormset(
        initial=init_values_motiv,
        prefix=formset_list[0], #get mcuser ones of last init meeting
    )
    vcontraindications_form = VContraindicationFormset(
        initial=init_values_contra,
        prefix=formset_list[1], #get mcuser ones of last init meeting
    )

    additional_personal_data_form = AdditionalPersonalDataForm(instance=additional_personal_data,
                                                               prefix='pers_data')

    formset_list = [{'prefix': prefix, 'url_post': ''}\
                    for prefix in formset_list]

    exams = []

    for SExam in Exam.__subclasses__():
        exam = {}
        exam['prefix'] = SExam.__name__
        exam['title'] = SExam._meta.verbose_name.title()
        exam['form_set'] = modelformset_factory(
            SExam, #getattr(cforms, f'{SExam.__name__}Form'),
            extra=5,
            can_delete=True,
            fields=SExam.get_ordered_fields(),
            exclude=('registered_by', 'registered_at'),
            widgets={'mc_user': HiddenInput()}
        )(
                queryset=SExam.objects.filter(mc_user=mcuser),
                prefix=exam['prefix'])

        # prefill mc_user and prescriber fields
        for f in exam['form_set']:
            f['mc_user'].initial = mcuser
            if f['prescriber'].initial is None:
                f['prescriber'].initial = request.user.as_doctor

        exams.append(exam)
        formset_list.append({'prefix': exam['prefix'],
                             'url_post': reverse('consultation:save_exam')})
        
    periods = []
    for Period in MCPeriod.__subclasses__():
        period = {}
        period['prefix'] = Period.__name__
        period['title'] = Period._meta.verbose_name#.title()
        period['form_set'] = modelformset_factory(
            Period,
            extra=5,
            can_delete=True,
            fields=Period.get_ordered_fields(),
            exclude = ('registered_by', 'registered_at'),
            widgets = {'mc_user': HiddenInput()}
        )(
                queryset=Period.objects.filter(mc_user=mcuser),
                prefix=period['prefix'])

        # prefill mc_user fields
        for f in period['form_set']:
            f['mc_user'].initial = mcuser

        periods.append(period)
        formset_list.append({'prefix': period['prefix'],
                             'url_post': reverse('consultation:save_period')})

    return render(request, 'consultation/start.html',
                  {'mcuser': mcuser,
                   'user_pk': pk_mcuser_as_user,
                   'user': request.user,
                   'formset_list': formset_list,
                   'additional_personal_data_form': additional_personal_data_form,
                   'vmotivations_form': vmotivations_form,
                   'vcontraindications_form': vcontraindications_form,
                   'user_serializer': user_serializer,
                   'exams': exams,
                   'periods': periods})


def save_additional_personal_data(req, pk_mcuser):
    if req.method != 'POST' or not req.is_ajax():
        return HttpResponse(status=status.HTTP_406_NOT_ACCEPTABLE)

    try:
        mcuser = MCUser.objects.get(pk=pk_mcuser)
    except MCUser.DoesNotExist:
        return HttpResponse('Non-existing MC user.')

    
    formset_list = ['motivation','contraindication']

    additional_personal_data_form = AdditionalPersonalDataForm(req.POST,
                                                               prefix='pers_data')

    VmotivationsFormset = formset_factory(ValuedMotivationForm,
                                          formset=ValuedMotivationFormSet,
                                          extra=5,
                                          can_delete=True)

    vmotivations_form = VmotivationsFormset(req.POST,
                                            prefix=formset_list[0])
    
    VcontraindicationsFormset = formset_factory(
        ValuedContraindicationForm,
        formset=ValuedContraindicationFormSet,
        extra=5,
        can_delete=True
    )

    vcontraindications_form = VcontraindicationsFormset(req.POST,
                                                prefix=formset_list[1])

    additional_personal_data = additional_personal_data_form.save(commit=False)
    additional_personal_data.mc_user = mcuser
    additional_personal_data.registered_by = req.user
    additional_personal_data.save()

    if vmotivations_form.is_valid():
        vmotivations_form.save(additional_personal_data.motivations)
    if vcontraindications_form.is_valid():
        vcontraindications_form.save(additional_personal_data.contraindications)

    return HttpResponse(status=status.HTTP_200_OK)


def save_exam(request):
    if request.method != 'POST' or not request.is_ajax():
        return HttpResponse(status=status.HTTP_406_NOT_ACCEPTABLE)

    for SExam in Exam.__subclasses__():
        prefix = SExam.__name__
        if not next(iter(request.POST.keys())).startswith(prefix):
            continue
        exam_form_set = modelformset_factory(
            SExam,
            fields=SExam.get_ordered_fields(),
            exclude = ('registered_by', 'registered_at'),
            can_delete=True)(request.POST,
                             prefix=prefix)
        if exam_form_set.is_valid():
            for cd in exam_form_set.cleaned_data:
                cd['registered_by'] = request.user.as_doctor
            exam_form_set.save()
        else:
            print(exam_form_set.errors)
            
    return HttpResponse(status=status.HTTP_200_OK)


def save_period(request):
    if request.method != 'POST' or not request.is_ajax():
        return HttpResponse(status=status.HTTP_406_NOT_ACCEPTABLE)

    for Period in MCPeriod.__subclasses__():
        prefix = Period.__name__
        if not next(iter(request.POST.keys())).startswith(prefix):
            continue
        period_form_set = modelformset_factory(
            Period,
            fields=Period.get_ordered_fields(),
            exclude = ('registered_by', 'registered_at'),
            can_delete=True)(request.POST,
                             prefix=prefix)
        if period_form_set.is_valid():
            for cd in period_form_set.cleaned_data:
                cd['registered_by'] = request.user.as_doctor
            period_form_set.save()
        else:
            print(period_form_set.errors)
            
    return HttpResponse(status=status.HTTP_200_OK)

