from django.shortcuts import render

from rest_framework.viewsets import ModelViewSet

from api.permissions import IsDoctor
from api.serializers import CustomUserSerializer, CustomShortUserSerializer
from api.filters import IsMCUserFilterBackend

from userauth.models import CustomUser, MCUser


class MCUserViewSet(ModelViewSet):
    permission_classes = [IsDoctor]
    filter_backends = [IsMCUserFilterBackend]

    def get_queryset(self):
        approx_last_name = self.request.GET.get('approx_last_name', None)
        queryset = CustomUser.objects.all()
        if approx_last_name is not None:
            queryset = queryset.filter(last_name__icontains=approx_last_name)
        return queryset

    def get_serializer_class(self):
        if self.action == 'list':
            return CustomShortUserSerializer
        else:
            return CustomUserSerializer

