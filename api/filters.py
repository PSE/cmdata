from rest_framework import filters

from userauth.models import MCUser

class IsMCUserFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """
    def filter_queryset(self, request, queryset, view):
        mcusers = MCUser.objects.all().values_list('user__id', flat=True)
        return queryset.filter(pk__in=mcusers)
