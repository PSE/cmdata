from rest_framework.permissions import BasePermission

class IsDoctor(BasePermission):
    message = 'All methods reserved for doctors'

    def has_permission(self, request, view):
        return request.user.is_authenticated  \
            and hasattr(request.user, 'as_doctor')
