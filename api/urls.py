from django.urls import path
from django.conf.urls import include

from rest_framework import routers, permissions

from api.views import MCUserViewSet

app_name = 'api'

routerUser = routers.SimpleRouter()

routerUser.register('mcuser', MCUserViewSet, basename='mcuser')

urlpatterns = [
    path('user/', include(routerUser.urls)),
]
