# from django_countries.serializers import CharField

from rest_framework import serializers
from userauth.models import CustomUser, MCUser
from django_countries.serializers import CountryFieldMixin

class CustomShortUserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = CustomUser
        fields = ('id', 'first_name', 'last_name', 'date_of_birth')


class CustomUserSerializer(CountryFieldMixin, serializers.ModelSerializer):
    
    class Meta:
        model = CustomUser
        fields = ('id', 'first_name', 'last_name', 'date_of_birth', 'ine_number', \
                  'address1', 'address2', 'zip_code', 'city', 'country', \
                  'additional_information',)


